package com.demo;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


import junit.framework.Assert;

@SpringBootTest
@RunWith(SpringRunner.class)
class SpringRestWithCassandraApplicationTests {
	
	@Test
	public void testGetAllStudent() {
		String url="http://localhost:9000/cassandra/getAll"; 	
		RestTemplate template=new RestTemplate();
		ResponseEntity<Object> res=template.exchange(url,HttpMethod.GET,null, Object.class);
		Assert.assertEquals(200, res.getStatusCodeValue());
	}
	
}
