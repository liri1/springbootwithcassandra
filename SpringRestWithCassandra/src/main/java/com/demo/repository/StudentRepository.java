package com.demo.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.demo.model.StudentModel;

@Repository
public interface StudentRepository extends CassandraRepository<StudentModel, Integer>{
	
}