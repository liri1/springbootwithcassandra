package com.demo.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.StudentModel;
import com.demo.service.StudentService;

@RestController
@RequestMapping("/cassandra")
public class StudentResource {
	@Autowired
	private StudentService studService;
	
	@RequestMapping("/getAll")
	public List<StudentModel> getAll(){
		return studService.getAllStudent();
	}
	
	@RequestMapping(value="/save",method = RequestMethod.POST)
	public String sava(@RequestBody StudentModel model) {
		return studService.saveStudent(model);
	}
	
	@RequestMapping(value = "/getById/{sno}",method = RequestMethod.GET)
	public StudentModel getStudentByID(@PathVariable("sno") String sno) {
		return studService.getStudentById(Integer.parseInt(sno));
	}

}
