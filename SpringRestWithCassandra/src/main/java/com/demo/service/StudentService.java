package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.StudentModel;
import com.demo.repository.StudentRepository;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studRepo;
	
	public String saveStudent(StudentModel stModel) {
		studRepo.save(stModel);
		return ((studRepo.save(stModel)!=null)?"Student Saved Successfully": "Save Operation Failed!!!");
	}
	
	public List<StudentModel> getAllStudent() {
		
		return studRepo.findAll();
	}
	
	public StudentModel getStudentById(int sno) {
		return studRepo.findById(sno).get();
	}

}
